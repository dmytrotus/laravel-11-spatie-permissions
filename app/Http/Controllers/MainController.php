<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Database\Factories\UserFactory;
use Spatie\Permission\Models\Permission;

class MainController extends Controller
{
    public function createUser(): User
    {
        return User::factory()->create([
            'name' => 'Test User',
            'email' => Str::random(5) . 'test@example.com',
        ]);
    }

    public function createRole(): string
    {
        $role = Role::create(['name' => 'dealer-admin']);
        $permission = Permission::create(['name' => 'add and delete all cars']);

        return 'Role and permission created';
    }

    public function addPermisionToTheRole(): string
    {
        $role = Role::where('name', 'dealer-admin')->firstOrFail();
        $permission = Permission::where('name', 'add and delete all cars')->firstOrFail();
        $role->givePermissionTo($permission);
        // or that way $permission->assignRole($role);

        // or remove permission
        // $role->revokePermissionTo($permission);

        return 'Asigned';
    }

    public function getUserRoles()
    {
        $user = User::first();
        return $user->getUserRoles();
    }
}
