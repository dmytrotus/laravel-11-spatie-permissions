<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/create-user', [MainController::class, 'createUser']);
Route::get('/create-role', [MainController::class, 'createRole']);
Route::get('/add-permission-to-the-role', [MainController::class, 'addPermisionToTheRole']);
Route::get('/roles', [MainController::class, 'getUserRoles']);
